import { SupportSQLiteCmds } from './SupportSQLiteCmds'
import { relationalStore } from '@kit.ArkData'

type SupportSQLiteCmdsType = SupportSQLiteCmds | Pick<SupportSQLiteCmds, any> | Omit<SupportSQLiteCmds, any>

export class RdbStoreHelper {
  constructor(private readonly rdbStore: relationalStore.RdbStore) {
  }

  /**
   * 执行 SQL 语句。
   * @param sql 要执行的 SQL 语句或 SupportSqliteCmds 对象。
   */
  executeSync(sql: string | SupportSQLiteCmdsType) {
    const exeSql = sql.toString()
    if (exeSql.length > 0) {
      if (sql instanceof SupportSQLiteCmds && !exeSql.endsWith(';')) {
        throw new Error('SupportSQLiteCmds is not complete.')
      }
      this.rdbStore.executeSql(exeSql)
    }
  }
}