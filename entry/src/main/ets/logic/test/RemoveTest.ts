import { myDatabase } from '../database/AppDatabase'
import { TableBook } from '../model/Book'
import { Bookcase } from '../model/Bookcase'

const bookcase: Bookcase = {
  name: "《月老馒不懂》"
}

export const RemoveTest = async function () {
  myDatabase.bookcaseDao
    .add(bookcase)
    .remove(bookcase) //移除数据

  return myDatabase.bookcaseDao.count(it => it.equalTo(TableBook.name, bookcase.name)) === 0
}