import { myDatabase } from '../database/AppDatabase'
import { Bookcase, TableBookcase } from '../model/Bookcase'

export const RemoveIfTest = async function () {
  const bookcase: Bookcase = {
    name: "科幻小说"
  }
  myDatabase.bookcaseDao
    .add(bookcase)
    .removeIf(it => it.equalTo(TableBookcase.name, "科幻小说")) //指定条件来删除数据

  return myDatabase.bookcaseDao.count() === 0
}