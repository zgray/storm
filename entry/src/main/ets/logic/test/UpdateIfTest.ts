import { myDatabase } from '../database/AppDatabase'
import { Bookcase, TableBookcase } from '../model/Bookcase'

export const UpdateIfTest = async function () {
  const bookcase: Bookcase = {
    name: "科幻小说"
  }
  myDatabase.bookcaseDao
    .add(bookcase)
    .updateIf(it => it.equalTo(TableBookcase.id, bookcase.id), { name: '女生小说' })

  return myDatabase.bookcaseDao.first(it => it.equalTo(TableBookcase.name, "女生小说")) !== undefined
}