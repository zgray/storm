import { myDatabase } from '../database/AppDatabase'
import { Bookcase, TableBookcase } from '../model/Bookcase'

export const UpdateTest = async function () {
  const bookcase: Bookcase = {
    name: "科幻小说"
  }
  myDatabase.bookcaseDao
    .add(bookcase)
    .begin(() => {
      bookcase.name = "女生小说"
      //修改 name 的值
    })
    .update(bookcase) //将修改后的值更新到数据库中

  return myDatabase.bookcaseDao.first(it => it.equalTo(TableBookcase.name, "女生小说")) !== undefined
}