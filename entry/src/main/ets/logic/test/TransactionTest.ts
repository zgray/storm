import { myDatabase } from '../database/AppDatabase'
import { Bookcase } from '../model/Bookcase'

export const TransactionTest = async function () {
  try {
    const bookcase: Bookcase = {
      name: "科幻小说"
    }
    myDatabase.bookcaseDao.beginTransaction(database => {
      database.add(bookcase)
      throw new Error('强制停止，让事务回滚')
    })
  } catch (e) {
    //...
  } finally {
    return myDatabase.bookcaseDao.count() <= 0
  }
}