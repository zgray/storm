import { Column, Table } from '@zxhhyj/storm';

export interface Album {
  id?: number
  name: string
  alias: string
}

export class AlbumTable extends Table<Album> {
  readonly tableName = 't_album'

  readonly id = Column.integer('id').primaryKey(true).bindTo(this, 'id')
  readonly name = Column.text('name').notNull().bindTo(this, 'name')
  readonly alias = Column.text('alias').bindTo(this, 'alias')
}

export const TableAlbum = new AlbumTable()