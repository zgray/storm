import { Database, DatabaseMigration, RdbStoreHelper, SupportSQLiteCmds, Table } from '@zxhhyj/storm';
import { Context } from '@kit.AbilityKit';
import { relationalStore } from '@kit.ArkData';

import { TableAlbum } from '../model/Album';

//一般情况不需要导出
export class TestDatabase extends Database {
  initDb(context: Context) {
    return relationalStore.getRdbStore(context, { name: "test.db", securityLevel: relationalStore.SecurityLevel.S1 })
  }

  readonly albumDao = TableAlbum
}

//一般情况不需要导出
export const Migration_0_NEW = new class Migration_0_NEW extends DatabaseMigration {
  readonly startVersion = 0
  readonly endVersion = NaN

  migrate(_tables: Table<any>[], _rdbStore: RdbStoreHelper): void {
    //什么也不做
  }
}

export const Migration_1_2 = new class Migration_1_2 extends DatabaseMigration {
  readonly startVersion = 1
  readonly endVersion = 2

  migrate(_tables: Table<any>[], rdbStore: RdbStoreHelper): void {
    rdbStore.executeSync(SupportSQLiteCmds.select(TableAlbum).alterTable().addColumn(TableAlbum.alias))
  }
}