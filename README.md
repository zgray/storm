### Storm

![language-ts](https://img.shields.io/badge/language-typescript-orange.svg)
[![license-Apache](https://img.shields.io/badge/license-Apache-blue)](LICENSE)
[![updates-更新日志](https://img.shields.io/badge/updates-%E6%9B%B4%E6%96%B0%E6%97%A5%E5%BF%97-brightgreen)](library/CHANGELOG.md)

### 介绍

`Storm`是直接基于纯`TypeScript`编写的高效简洁的轻量级`OpenHarmonyOS SQL ORM`框架，提供了`强类型`的`SQL DSL`
，直接将低级`bug`暴露在编译期。

### 使用文档

[传送门](library/README.md)。

### 参与贡献

欢迎提`Issues`和`PR`，你的`Issues`就是我最大的动力，你的`PR`是对我最大的支持。